Miscellaneous scripts, notes, etc pertaining to HPR episodes that I have contributed

## Update

Now added to the group `oss_tools`

- Originally: `git@gitlab.com:davmo/hprmisc.git`
- Now: `git@gitlab.com:oss_tools/hprmisc.git`
